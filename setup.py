# coding: utf-8

import sys
from setuptools import setup, find_packages

NAME = "variable_misuse"
VERSION = "0.0.1"
# To install the library, run the following
#
# python setup.py install
#
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

REQUIRES = ["connexion"]

setup(
    name=NAME,
    version=VERSION,
    description="API for running variable misuse",
    author="Marcos Fernández, Eduardo Martín, Pablo de Castro",
    author_email="marcos.fernandez@treelogic.com, eduardo.martin@treelogic.com, pablo.castro@treelogic.com",
    url="https://www.decoder-project.eu",
    keywords=["Swagger", "WP2_parse_source_code"],
    install_requires=REQUIRES,
    packages=find_packages(),
    package_data={'': ['swagger/swagger.yaml']},
    include_package_data=True,
    entry_points={
        'console_scripts': ['swagger_server=swagger_server.__main__:main']},
    long_description="""\
    This covers both the Variable Misuse (T2.3) task.
    """
)
