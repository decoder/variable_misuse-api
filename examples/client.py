import requests


# Configuration parameters
base_url = 'http://localhost:8081/apt/predictions/'
collection_example = 'OpenCV'
filename_example = '001.cpp'
url_all = base_url + collection_example + '/code_summarization' # For fetching all files within a collection
url = base_url + collection_example + '/code_summarization/' + filename_example # For fetching a single filename from a collection


def check_reply(reply):
    if (r.ok):
        print('Result from the request in json: ', r.json())
    else:
        # If response code is not ok (200), print the resulting http error code with description
        r.raise_for_status()


# Simple request
r = requests.post(url)
check_reply(r) 

# Multiple request
r = requests.post(url_all)
check_reply(r) 


