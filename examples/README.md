# Clients for interacting with the code summarization and PKM APIs
This folder contains two client examples in Python in order to interact with the DECODER code summarization and variable misuse API, as well as the low-level PKM API.


## Overview
The clients make use of the requests Python library in order to make requests to the two APIs. The available clients are:
* client.py: This client interacts with the code summarization and variable misuse API. Contains the code for annotating an entire collection as well as a single filename inside that collection. 
* client_pkm.py: This client interacts with the PKM low-level API. The example provided contains the code for user login, user and project creation, insertion of a source code file into a project, and retrieval of a single and all the source code files within a project.

