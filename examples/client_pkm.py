import requests
requests.packages.urllib3.disable_warnings()


# Configuration parameters for PKM
base_url = 'https://pkm-api_pkm_1:8080'
collection_example = 'mythaistar2'

# Configuration parameters for local files
relative_path = '../swagger_server/resources/code/input/My-Thai-Star/' # For loading local files
filenames = ['BinaryObjectEto_5.java', 'OrderedDishesCto_3.java', 'PredictionDayDataCto_4.java', 'UserRoleSearchCriteriaTo_7.java', 'CategoryEto_4.java']


def check_reply(reply):
    if (reply.ok):
        try:
            print('Result from the request in json: ', reply.json())
            return reply.json()
        except:
            pass
    else:
        """
        if reply.status_code==403:
            url = r.url
            element = url.split('/')[-1]
            print('%s already exists' %element)
        """
        if reply.status_code==409:
            print('Error %s %s' %(reply.status_code, reply.reason))
        else:
            # If response code is not ok (200) or 404 print the resulting http error code with description
            reply.raise_for_status()


# Admin user login
login_url = base_url + '/user/login'
payload = {'user_name': 'admin', 'user_password': 'admin'}
headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

print('Login as admin user...')
try:
    r = requests.post(login_url, json=payload, headers=headers, verify='pkm.crt')
    json_reply = check_reply(r)
except:
    raise
admin_key = json_reply['key']


"""
# User creation
user_creation_url = base_url + '/user'
payload = {'name': 'marcos', 'password': '12345'}
headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'key': key}

print('\nCreating a new user...')
try:
    r = requests.post(user_creation_url, json=payload, headers=headers, verify='pkm.crt')
    json_reply = check_reply(r)
except:
    raise
"""


# Project creation
project_creation_url = base_url + '/project'
payload = {'name': collection_example, 'members': [{"name": "admin", "owner": 'True', "roles":["Developer"]}]}
headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'key': admin_key}

print('\nCreating collection %s...' %collection_example)
try:
    r = requests.put(project_creation_url, json=payload, headers=headers, verify='pkm.crt')
    check_reply(r)
except:
    raise


"""
# Login with the user created
user_login_url = base_url + '/user/login'
payload = {'user_name': 'marcos', 'user_password': '12345'}
headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}

print('\nLogin as user...')
try:
    r = requests.post(user_login_url, json=payload, headers=headers, verify='pkm.crt')
    json_reply = check_reply(r)
except:
    raise
user_key = json_reply['key']
"""


# Insert source code files in PKM
insert_file_url = base_url + '/code/rawsourcecode/' + collection_example
headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'key': admin_key}

print('\nInserting filenames into collection %s...' %collection_example)
for filename in filenames:
    # Load the content in filename and pass it to the payload
    with open(relative_path + filename, "r") as file:
        content = file.read().rstrip() # Read every code file into a string
    payload = [{'format': 'text', 'encoding': 'utf-8', 'rel_path': filename, 'content': content}]

    print('Inserting filename %s into PKM...' %filename)
    try:
        r = requests.put(insert_file_url, json=payload, headers=headers, verify='pkm.crt')
        json_reply = check_reply(r)
    except:
        raise


# Retrieve a single source code file from a project
get_file_url = base_url + '/code/rawsourcecode/' + collection_example + '/' + filenames[0]
headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'key': admin_key}

print('\nRetrieving %s file from collection %s...' %(filenames[0], collection_example))
try:
    r = requests.get(get_file_url, headers=headers, verify='pkm.crt')
    json_reply = check_reply(r)
except:
    raise

print('Retrieved filename %s with content: %s' %(json_reply['rel_path'], json_reply['content']))


# Retrieve all source code files from a project
get_file_url = base_url + '/code/rawsourcecode/' + collection_example
headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'key': admin_key}

print('\nRetrieving all files from collection %s...' %collection_example)
try:
    r = requests.get(get_file_url, headers=headers, verify='pkm.crt')
    json_reply = check_reply(r)
except:
    raise

filenames = []
code = []
for i in range(len(json_reply)):
    filenames.append(json_reply[i]['rel_path'])
    code.append(json_reply[i]['content'])
    print('Retrieved filename %s with content: %s' %(filenames[i], code[i]))


