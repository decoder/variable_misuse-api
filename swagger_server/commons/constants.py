from pathlib import Path
import logging
import os

logger = logging.getLogger("DECODER_API.controller")


# Paths
DEFAULT_RESOURCES_PATH = (
    Path(__file__).resolve() / "../../resources/"
).resolve().as_posix() + "/"
LOG_PATH = "swagger_server/logs/DECODER_API.log"

# Models
JAVA_POINTER = 'models/Java/model.h5'
C_POINTER = 'models/C/model.h5'

# Dictionaries
TOKEN2IDX_JAVA = 'dicts/token2idx_java.json'
TOKEN2IDX_C = 'dicts/token2idx_c.json'

# Input and output lengths
MAX_LENGTH_CODE = 200

# Inference probability threshold
THRESHOLD=0.4

# Common config
LOGGING_LEVEL = "INFO"

PKM_API_ADDRESS = os.environ.get("PKM_API_ADDRESS")

if not PKM_API_ADDRESS:
	logging.warning("PKM_API_ADDRESS env_var was not specified, using localhost as default.")
	PKM_API_ADDRESS = 'localhost'

PKM_API_PORT = '8080'
PKM_API_BASE_URL = 'http://' + PKM_API_ADDRESS + ':' + PKM_API_PORT
PKM_API_RAW_PATH = PKM_API_BASE_URL + '/code/rawsourcecode/'
PKM_API_JAVA_SOURCE_PATH = PKM_API_BASE_URL + '/code/java/sourcecode/'
PKM_API_C_SOURCE_PATH = PKM_API_BASE_URL + '/code/c/sourcecode/'
PKM_API_CPP_SOURCE_PATH = PKM_API_BASE_URL + '/code/cpp/sourcecode/'
