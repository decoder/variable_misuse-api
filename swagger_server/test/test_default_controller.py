# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501
from swagger_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_app_predictions_code_summ_java(self):
        """Test case for app_predictions_code_summ_java

        Predicts natural language description given source code files in Java
        """
        response = self.client.open(
            '/apt//predictions/code_summarization/Java/{artefactId}'.format(artefact_id='artefact_id_example'),
            method='POST')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
