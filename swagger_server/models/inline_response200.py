# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server import util


class InlineResponse200(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, artefact_id: str=None, modified: str=None):  # noqa: E501
        """InlineResponse200 - a model defined in Swagger

        :param artefact_id: The artefact_id of this InlineResponse200.  # noqa: E501
        :type artefact_id: str
        :param modified: The modified of this InlineResponse200.  # noqa: E501
        :type modified: str
        """
        self.swagger_types = {
            'artefact_id': str,
            'modified': bool
        }

        self.attribute_map = {
            'artefact_id': 'artefactId',
            'modified': 'NLDescription'
        }
        self._artefact_id = artefact_id
        self._modified = modified

    @classmethod
    def from_dict(cls, dikt) -> 'InlineResponse200':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The inline_response_200 of this InlineResponse200.  # noqa: E501
        :rtype: InlineResponse200
        """
        return util.deserialize_model(dikt, cls)

    @property
    def artefact_id(self) -> str:
        """Gets the artefact_id of this InlineResponse200.


        :return: The artefact_id of this InlineResponse200.
        :rtype: str
        """
        return self._artefact_id

    @artefact_id.setter
    def artefact_id(self, artefact_id: str):
        """Sets the artefact_id of this InlineResponse200.


        :param artefact_id: The artefact_id of this InlineResponse200.
        :type artefact_id: str
        """

        self._artefact_id = artefact_id

    @property
    def modified(self) -> str:
        """Gets the modified of this InlineResponse200.


        :return: The modified of this InlineResponse200.
        :rtype: str
        """
        return self._modified

    @modified.setter
    def modified(self, modified: str):
        """Sets the modified of this InlineResponse200.


        :param modified: The modified of this InlineResponse200.
        :type modified: str
        """

        self._modified = modified
