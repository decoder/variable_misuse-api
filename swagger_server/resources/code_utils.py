import io
from typing import Dict, List, Tuple

import pygments
import pygments.token
import pygments.lexer
from pygments.formatters import NullFormatter
from pygments.lexers import CLexer, CppLexer, JavaLexer
from typing_extensions import Literal
import requests
import logging 
import time 
from swagger_server.commons.constants import DEFAULT_RESOURCES_PATH, MAX_LENGTH_CODE, THRESHOLD
import numpy as np
from collections import Counter
import re 

# Load the logger
logger = logging.getLogger("DECODER_API.core.model")

# definition of some types
SupportedLanguages = Literal["java", "c", "cpp"]

# some useful global constant variables
LEXER_DICT: Dict[SupportedLanguages, pygments.lexer.Lexer] = {
    "java": JavaLexer(),
    "c": CLexer(),
    "cpp": CppLexer(),
}
FORMATTER = NullFormatter()

def split_methods_java(ast_dicts: List[Dict], raw_source_codes: List[str]):
    """
    Splits Java source code files into different methods based on Javaparser AST.

    Parameters
    ----------
    ast_dicts: List[Dict]
        List of dicts with the Javaparser AST information of each of the files.
    raw_source_codes: List[str]
        List of strings containing the different source code files.

    Returns
    ----------
    dict_method_info: Dict
        Dictionary mapping each file to info (tuple of list of method codes and line of start for each method)
    """

    check_matching_sizes(ast_dicts, raw_source_codes)

    dict_method_info = {}
    for ast_dict, raw_source_code in zip(ast_dicts, raw_source_codes):
        logger.info("Slicing file %s..." % ast_dict["sourceFile"])
        raw_code_by_lines = raw_source_code.splitlines(keepends=True)

        # this returns a list of declared methods in each file
        method_asts = ast_dict["CompilationUnit"]["TypeDeclaration"][
            "MethodDeclaration"
        ]
        method_info_in_file = []
        num_methods = len(method_asts)
        logger.info("Number of methods in original file: %d" % num_methods)
        if num_methods > 0:

            # extract each method (in reverse order is simpler)
            method_end_line = len(raw_code_by_lines)

            # wrap in a list if not list already
            # important to deal with the case of a single method
            # inside a class such as the main method of an app
            if not isinstance(method_asts, list):
                method_asts = [method_asts]

            for method_ast in method_asts[::-1]:
                
                # this is the starting line including the javadoc if any
                method_start_line_all = method_ast["loc"]["pos_l"] - 1

                # init start line where name is in the declaration
                method_start_line = method_ast["SimpleName"]["loc"]["pos_l"] - 1
                # check if other elements that are not doc start earlier
                for el_key, el_value in method_ast.items():
                    if el_key in ("loc", "Javadoc"):
                        pass
                    # skip lists (e.g. variable declarations)
                    elif isinstance(el_value, dict):
                        el_start_line = el_value["loc"]["pos_l"] - 1
                        if el_start_line < method_start_line:
                            method_start_line = el_start_line

                # get method code from file
                method_code = "".join(
                    raw_code_by_lines[method_start_line:method_end_line]
                )
                method_code = clean_after_method_code(method_code, "java")

                #info containing method code and start line of method
                method_info_in_file.append((method_code, method_start_line))

                # set start as next end line (comments in between will be ignored later)
                method_end_line = method_start_line_all

            # reverse back to get order
            dict_method_info[ast_dict["sourceFile"]] = method_info_in_file[::-1]
        else:
            logger.warning(
                "File %s has no annotated methods in the PKM, the entire file will be analyzed."
                % ast_dict["sourceFile"]
            )
            dict_method_info[ast_dict["sourceFile"]] = [(raw_source_code, 0)]
        


    return dict_method_info


def split_methods_c(ast_dicts: List[Dict], raw_source_codes: List[str]):
    """
    Splits C source code files into different methods based on Frama C AST.

    Parameters
    ----------
    ast_dicts: List[Dict]
        List of dicts with the Frama C AST information of each of the files.
    raw_source_codes: List[str]
        List of strings containing the different source code files.

    Returns
    ----------
    dict_method_info: Dict
        Dictionary mapping each file to info (tuple of list of method codes and line of start for each method)
    """

    check_matching_sizes(ast_dicts, raw_source_codes)

    dict_method_info = {}
    for ast_dict, raw_source_code in zip(ast_dicts, raw_source_codes):

        logger.info("Slicing file %s..." % ast_dict["sourceFile"])

        raw_code_by_lines = raw_source_code.splitlines(keepends=True)

        method_info_in_file = []
        for ast_obj in ast_dict["globals"]:

            # check if it is a function
            if "GFun" in ast_obj:

                method_start_line = ast_obj["GFun"]["loc"]["pos_start"]["pos_lnum"] - 1
                method_end_line = ast_obj["GFun"]["loc"]["pos_end"]["pos_lnum"] - 1

                method_code = "".join(
                    raw_code_by_lines[method_start_line : method_end_line + 1]
                )

                method_info_in_file.append((method_code, method_start_line))

        dict_method_info[ast_dict["sourceFile"]] = method_info_in_file

    return dict_method_info


def iterate_over_ast_dict(ast_dict, raw_code_by_lines, method_info_in_file):
    """
    Iterate over AST dict for C++ in a recursive mode.

    Parameters
    ----------
    ast_dict: Dict
        Ast dict with the script parsed by frama-clang
    raw_code_by_lines: List[str]
        List of strings containing the different source code lines.
    method_info_in_file: List[tuple]
        List of tuples containing method code and numbers of line for methods.
    """

    for ast_obj in ast_dict["inner"]:
        # check if it is a function
        if ast_obj["kind"] in (
            "CXXConstructorDecl",
            "CXXDestructorDecl",
            "CXXMethodDecl",
            "FunctionDecl"
        ):

            method_start_line = ast_obj["range"]["begin"]["line"] - 1
            method_end_line = ast_obj["range"]["end"]["line"] - 1

            method_code = "".join(
                raw_code_by_lines[method_start_line : method_end_line + 1]
            )

            #avoiding duplicates
            methods_start_line = [method_info[1] for method_info in method_info_in_file]
            if method_start_line not in methods_start_line:
                method_info_in_file.append((method_code, method_start_line))
        
        #going lower level if there is inner data 
        if ast_obj["kind"] in ('NamespaceDecl','CXXRecordDecl'):
            if ("inner" in ast_obj) and ast_obj['inner']:
                iterate_over_ast_dict(ast_obj, raw_code_by_lines, method_info_in_file)


def split_methods_cpp(ast_dicts: List[Dict], raw_source_codes: List[str]):
    """
    Splits CPP source code files into different methods based on Frama Clang AST.

    Parameters
    ----------
    ast_dicts: List[Dict]
        List of dicts with the Frama Clang AST information of each of the files.
    raw_source_codes: List[str]
        List of strings containing the different source code files.

    Returns
    ----------
    dict_method_info: Dict
        Dictionary mapping each file to info (tuple of list of method codes and line of start for each method)
    """

    check_matching_sizes(ast_dicts, raw_source_codes)

    dict_method_info = {}
    for ast_dict, raw_source_code in zip(ast_dicts, raw_source_codes):

        logger.info("Slicing file %s..." % ast_dict["sourceFile"])

        raw_code_by_lines = raw_source_code.splitlines(keepends=True)

        method_info_in_file = []
        iterate_over_ast_dict(ast_dict, raw_code_by_lines, method_info_in_file)
        
        dict_method_info[ast_dict["sourceFile"]] = method_info_in_file

    return dict_method_info


def clean_after_method_code(method_code: str, code_language: SupportedLanguages) -> str:
    """Returns a copy of the method without spaces, new lines and comments at the end."""

    # get list of tokens in the code
    original_token_list: List[Tuple[pygments.token.Token, str]] = list(
        pygments.lex(method_code, LEXER_DICT[code_language])
    )

    # iterate in reverse order to find index of end of method
    # that is not a space, whitespace or comment)
    end_of_method = len(original_token_list)
    for token_type, token_str in reversed(original_token_list):

        # remove if is comment
        if token_type in pygments.token.Comment:
            end_of_method -= 1
        # remove if is blank, space, newline or tab (text in general at the end)
        elif token_type in pygments.token.Text:
            end_of_method -= 1
        # otherwise break the loop
        else:
            break

    # trim tokens and add new line at the end
    modified_token_list = original_token_list[:end_of_method]
    modified_token_list.append((pygments.token.Text, "\n"))

    file_like = io.StringIO()
    FORMATTER.format(modified_token_list, file_like)

    # seed and read to get string
    file_like.seek(0)
    modified_code = file_like.read()

    return modified_code


def check_matching_sizes(ast_dicts, raw_source_codes):

    if len(ast_dicts) != len(raw_source_codes):
        logger.info(
            "Number of files in ast collection: %d",
            len(ast_dicts),
        )
        logger.info(
            "Number of files in RawSourcecode collection: %d", len(raw_source_codes)
        )
        raise Exception(
            "Number of files mismatch between RawSourcecode and ast collections"
        )


def filter_ast_dicts(ast_dicts: List[Dict], filenames: List[str]):

    filtered_ast_dicts = []
    for filename in filenames:
        for ast_dict in ast_dicts:
            if filename == ast_dict["sourceFile"]:
                filtered_ast_dicts.append(ast_dict)

    return filtered_ast_dicts


class DebugHelper:

    def __init__(self):
        self.max_len_code = MAX_LENGTH_CODE

    def get_debug_info(self, dict_methods_info, predictions, tokenization_df):
        """
        Get debug information per filename on dictionary format

        Parameters
        ----------
        dict_methods_info: Dict
            List of dicts with the Frama Clang AST information of each of the files.
        predictions: numpy.ndarray
            Raw prediction obtained from model
        tokenization_df: DataFrame
            Dataframe with each token associated with its tag and filename

        Returns
        ----------
        dict_bigs_info: Dict
            Dictionary mapping each file with a list of bugs
        modified_methods_in_files: List[List[bool]]
            List of lists representing changes in method codes of filenames
        )
        """
        filenames = dict_methods_info.keys()
        
        dict_bugs_info = {}
        modified_methods_in_files: List[List[bool]] = []
        cnt_predictions = 0

        for filename in filenames:
            methods_info = dict_methods_info[filename]
            bugs_info = [] 
            modified_methods = []
            for i, method_info in enumerate(methods_info):
                modified = False
                filtered_df = tokenization_df[tokenization_df['method_id']==f'{filename}-{i}'] #select data for specific method
                method_predictions = predictions[cnt_predictions]

                prediction_pointer = np.argmax(method_predictions, axis=-1)
                repair_token_prob = np.max(method_predictions, axis=-1)[1]

                cnt_predictions += 1 

                #debug method
                bug_info = self._debug_method(prediction_pointer, repair_token_prob, filtered_df, method_info, filename, i)

                #Only add bug info when there is a bug in the method
                if bug_info:
                    bugs_info.append(bug_info)
                    modified = True
                
                modified_methods.append(modified)

            dict_bugs_info[filename] = bugs_info
            modified_methods_in_files.append(modified_methods)

        return dict_bugs_info, modified_methods_in_files

    def add_prediction_comments(self, dict_bugs_info, raw_source_codes):
        """
        Add comments to raw source codes

        Parameters
        ----------
        dict_bigs_info: Dict
            Dictionary mapping each file with a list of bugs
        raw_source_codes: List[List[str]]
            Raw source codes associated to each script

        Returns
        ----------
        modified_raw_source_codes: List[List[str]]
            Raw source codes with the commented lines added
        )
        """
        # iterate for each file
        modified_raw_source_codes: List[str] = []

        for filename, raw_source_code in zip(dict_bugs_info.keys(), raw_source_codes):
            bug_info_list = dict_bugs_info[filename]
            modified_raw_source_code: str = ""
            #if the list of bugs is empty, there is no bugs
            if bug_info_list:
                file_method_line_numbers = [bug_method_info['line_number_location'] for bug_method_info in bug_info_list]
                located_token_bugs = [bug_method_info['wrong_token'] for bug_method_info in bug_info_list]
                repair_token_bugs = [bug_method_info['replace_token'] for bug_method_info in bug_info_list]
                
                for line_number, line_content in enumerate(raw_source_code.splitlines(keepends=True)):
                    # if line is start of method, add description
                    if line_number in file_method_line_numbers:
                        bug_index = file_method_line_numbers.index(line_number)
                        located_token = located_token_bugs[bug_index]
                        repair_token = repair_token_bugs[bug_index]
                        
                        line_content = self._add_documentation(line_content, located_token, repair_token)

                    modified_raw_source_code += line_content

            else: 
                modified_raw_source_code = raw_source_code
            
            modified_raw_source_codes.append(modified_raw_source_code)
        
        return modified_raw_source_codes

    def _add_documentation(self, line_content, located_token, repaired_token):
        comment = f''' // DECODER-VARIABLE_MISUSE: Check if replacing '{located_token}' by '{repaired_token}' needed.'''

        #if there is break lines, insert one at the end of the comment
        if re.findall('(\\n)', line_content):
            line_content = line_content.replace('\n', '')
            comment += '\n'

        return line_content + comment

    def _debug_method(self, prediction_pointer, repair_token_prob, filtered_df, method_info, filename, index_method):
        bug_info = {}

        raw_method_code, method_start_line_number = method_info
        method_tokens = filtered_df['token'].values.tolist()
        method_tags = filtered_df['tags'].values.tolist()
        method_line_numbers = filtered_df['line_number'].values.tolist()

        location_index = prediction_pointer[0] # Cardinal position of the bug within file_tokens
        repair_index = prediction_pointer[1] # Cardinal position of the repair within method_tokens

        """
            Requirements for considering a bug:
                1. The Location index must be greater than 0
                2. The number of tokens must be less than 200 (max_length_code)
                3. There must be more than one unique variable
                4. The most frequent variable must appear at least 2 times
                5. The location token must be different to the repair token
                6. The location and repair tokens should be of Variable type
                7. The repair max probability should be greater than a threshold
        """

        if (location_index!=0) and \
           (len(method_tokens)<=MAX_LENGTH_CODE) and \
            all(prediction_pointer < len(method_tokens)):

            token_variables = [token for index, token in enumerate(method_tokens) if method_tags[index]=='Name'] # List with all the variables in the original method                   
            var_counts = Counter(token_variables)

            if len(var_counts) > 1:
                _, num_most_common = var_counts.most_common(1)[0]

                if num_most_common > 1:

                    location_token = method_tokens[location_index]
                    repair_token = method_tokens[repair_index]

                    location_tag = method_tags[location_index]
                    repair_tag = method_tags[repair_index]
                    location_line_number = method_line_numbers[location_index]

                    if location_token != repair_token and \
                      (location_tag == 'Name') and (repair_tag == 'Name') and \
                        repair_token_prob > THRESHOLD:

                        line_number_bug_location = method_start_line_number + location_line_number

                        #save bug info
                        bug_info['line_number_location'] = line_number_bug_location - 1 
                        bug_info['wrong_token'] = location_token
                        bug_info['replace_token'] = repair_token

                        logger.info(f'Bug detected in method {index_method+1} of file: {filename}')
                        logger.info(f'Original method code: \n {raw_method_code}')
                        logger.info(f'Script line: {line_number_bug_location}; Method line: {location_line_number}')
                        logger.info(f'Bug token: {location_token}')
                        logger.info(f'Repair token: {repair_token}')
                            

        return bug_info                

class PkmHelper:

    def __init__(self):
        self.base_path = DEFAULT_RESOURCES_PATH

    def get(self, url, key):
        start = time.time()
        headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'key': key} 

        logger.info('Retrieving file from URL %s...' %(url))
        try:
            r = requests.get(url, headers=headers, verify=self.base_path+'pkm.crt')
            json_reply = self._check_reply(r, 'get')
        except requests.exceptions.HTTPError as err:
            logger.exception(err)
            raise

        end = time.time()
        logger.info('Download time: %s seconds\n' %(end-start))

        return json_reply

    def put(self, url, payload, key):
        start = time.time()
        headers = {'Content-Type': 'application/json', 'Accept': 'application/json', 'key': key}  

        logger.info('Inserting file in URL %s...' %(url))
        try:
            # Insert (if not yet present) or update (if present) source code file into the PKM 
            r = requests.put(url, json=payload, headers=headers, verify=self.base_path+'pkm.crt')
            self._check_reply(r, 'put')
        except requests.exceptions.HTTPError as err:
            logger.exception(err)
            raise
        end = time.time()
        logger.info('Total time for insertion: %s seconds\n' %(end-start))

    def _check_reply(self, reply, method):
        if (reply.ok) or (method == 'get' and reply.status_code == 404):
            try:
                return reply.json()
            except:
                pass
        else:
            # If response code is not ok (200) raise the error
            reply.raise_for_status()