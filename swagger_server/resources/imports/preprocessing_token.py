import re

import pygments
from pygments.lexers import JavaLexer
from pygments.lexers.c_cpp import CLexer, CppLexer

import time
import pandas as pd
import numpy as np

from tensorflow.keras.preprocessing.sequence import pad_sequences


def tokenization(dict_methods_info, lexer=CppLexer()):
    """
    Gets the list of tokens related to a script or a list of scripts.

    Parameters
    ----------
    dict_method_info: Dict
        Dictionary mapping each file to info (tuple of list of method codes and line of start for each method)
    lexer: pygments.lexer.Lexer
        Lexer instance for tokenization

    Returns
    ----------
    dicts_methods_token_list: Dict
        Dictionary mapping each method id with its token info
    """

    ### SUB-FUNCTIONS definitions
    
    ##Apply Pigments
    def TokenizeList(method_codes):
        token_list = []
        
        for snippet in method_codes:
            result = pygments.lex(str(snippet), lexer)
            token_list.append(list(result))

        #get line number per token on each method
        number_line_per_token = []

        for method_index in range(len(method_codes)):
            method_token_list = token_list[method_index]
            cnt_lines = 1
            method_line_per_token = []
            #get line number for each token of method
            for _, token in method_token_list:
                method_line_per_token.append(cnt_lines)

                n_line_breaks = len(re.findall('(\\n)', token))
                cnt_lines += n_line_breaks

            number_line_per_token.append(method_line_per_token)

        
        return token_list, number_line_per_token
    
    #Join double symbols and -(number) 
    def JoinDoubleSymbols(token_list, number_line_per_token):
    
        new_token_list = []
        new_number_line_per_token = []
        modified = 0

        for method_idx, method_tokens_list in enumerate(token_list):

            inner_token_list = []
            inner_new_number_line_per_token = []

            for token_idx, (first, second) in enumerate(zip(method_tokens_list, method_tokens_list[1:])):

                if str(first[0]).startswith('Token.Text') == False:

                    if str(first[1]) != '\n':

                        if modified == 0:

                            if first == second and first[1] == ':':
                                first = (first[0], '::')
                                modified = modified + 1

                            elif first == second and first[1] == '=':
                                first = (first[0], '==')
                                modified = modified + 1

                            elif first[1] == '!' and second[1] == '=':
                                first = (first[0], '!=')
                                modified = modified + 1

                            elif first[1] == '-' and second[1] == '>':
                                first = (first[0], '->')
                                modified = modified + 1

                            elif first[1] == '+' and second[1] == '+':
                                first = (first[0], '++')
                                modified = modified + 1

                            elif first[1] == '-' and second[1] == '-':
                                first = (first[0], '--')
                                modified = modified + 1

                            elif first[1] == '&' and second[1] == '&':
                                first = (first[0], '&&')
                                modified = modified + 1

                            elif first[1] == '|' and second[1] == '|':
                                first = (first[0], '||')
                                modified = modified + 1

                            elif first[1] == '-' and str(second[0]) == 'Token.Literal.Number.Integer':
                                first = (second[0], '-'+str(second[1]))
                                modified = modified + 1

                            inner_token_list.append(first)
                            inner_new_number_line_per_token.append(number_line_per_token[method_idx][token_idx])

                        else:
                            modified = 0


            new_token_list.append(inner_token_list)
            new_number_line_per_token.append(inner_new_number_line_per_token)

        return new_token_list, new_number_line_per_token

    ## Identify empty strings ("") and change single word strings from '"' + word + '"' to '"word"'
    def SingleStrings(token_list, number_line_per_token):
    
        new_token_list = []
        new_number_line_per_token = []

        for method_index in range(len(token_list)):

            inner_token_list = []
            inner_new_number_line_per_token = []
            method_token_list = token_list[method_index]

            for token_index in range(len(method_token_list)):

                token_tag = method_token_list[token_index][0]
                token_content = method_token_list[token_index][1]
                token_line_number = number_line_per_token[method_index][token_index]

                if str(token_tag) != 'Token.Literal.String.Escape':                    
                    if str(token_tag) == ('Token.Literal.String.Char'):

                        if str(token_content) == "'":

                            if str(method_token_list[token_index+1][1]) != "'":

                                if str(method_token_list[token_index+2][1]) == "'":

                                    inner_token_list.append((token_tag, '"'+str(method_token_list[token_index+1][1])+'"'))
                                    inner_new_number_line_per_token.append(token_line_number)
                        else:
                            continue
                            
                    

                    elif str(token_tag) == ('Token.Literal.String'):

                        if str(token_content)== '"':

                            if str(method_token_list[token_index+1][1])== '"':

                                if str(method_token_list[token_index+2][0]).startswith('Token.Literal.String') == False or str(method_token_list[token_index-1][0]).startswith('Token.Literal.String')==False:

                                    inner_token_list.append((token_tag, '""'))
                                    inner_new_number_line_per_token.append(token_line_number)
                                    


                        else:
                            inner_token_list.append((token_tag,'"'+str(token_content)+ '"'))
                            inner_new_number_line_per_token.append(token_line_number)
                    else:
                        inner_token_list.append(method_token_list[token_index])
                        inner_new_number_line_per_token.append(token_line_number)                

            new_token_list.append(inner_token_list)
            new_number_line_per_token.append(inner_new_number_line_per_token)

        return new_token_list, new_number_line_per_token
    
    
    ## Join all the other strings (more than one word) into the first 
    def JoinStrings(token_list, number_line_per_token):
        
        new_token_list = []
        new_number_line_per_token = []
        modified = 0
        
        for method_idx, method_tokens_list in enumerate(token_list):
            
            inner_token_list = []
            inner_new_number_line_per_token = []
            last_token = method_tokens_list[-1]

            method_tokens_line_number = number_line_per_token[method_idx]
            last_token_number = method_tokens_line_number[-1]
            
            for token_idx, (first, second) in enumerate(zip(method_tokens_list, method_tokens_list[1:])):

                if modified == 0:

                    if str(first[0]).startswith('Token.Literal.String') and str(second[0]).startswith('Token.Literal.String'): 
                        first = ((first[0], str(first[1]+second[1])))
                        modified = modified + 1


                    inner_token_list.append(first)
                    inner_new_number_line_per_token.append(method_tokens_line_number[token_idx])
                    
                else:
                    modified = 0
                    
            inner_token_list.append(last_token)
            inner_new_number_line_per_token.append(last_token_number)

            new_token_list.append(inner_token_list)
            new_number_line_per_token.append(inner_new_number_line_per_token)
            
        return new_token_list, new_number_line_per_token
    
    def DiscardLabelTokenString(token_list, number_line_per_token):
        new_token_list = []
        new_number_line_per_token = []
        for method_idx in range(len(token_list)):
            inner_token_list = []
            inner_new_number_line_per_token = []
            for token_idx in range(len(token_list[method_idx])):
                if str(token_list[method_idx][token_idx][0]).startswith('Token'):
                    inner_token_list.append((re.sub('Token.', '',str(token_list[method_idx][token_idx][0])),token_list[method_idx][token_idx][1]))
                    inner_new_number_line_per_token.append(number_line_per_token[method_idx][token_idx])

            new_token_list.append(inner_token_list)
            new_number_line_per_token.append(inner_new_number_line_per_token)

        return new_token_list, new_number_line_per_token
    
    dicts_methods_token_list = {}

    #for each file we take its tokens
    for filename in dict_methods_info.keys():
        methods_info = dict_methods_info[filename]
        method_codes = [method_info[0] for method_info in methods_info]

        # obtain the results
        list_tokens, number_line_per_token = TokenizeList(method_codes)
        list_tokens, number_line_per_token = JoinDoubleSymbols(list_tokens, number_line_per_token)
        list_tokens, number_line_per_token = SingleStrings(list_tokens, number_line_per_token)
        list_tokens, number_line_per_token = JoinStrings(list_tokens, number_line_per_token)
        list_tokens, number_line_per_token = DiscardLabelTokenString(list_tokens, number_line_per_token)

        #maps each method with its tokens
        for method_idx, (list_tokens_method, list_number_line_tokens_method) in enumerate(zip(list_tokens, number_line_per_token)):
            dicts_methods_token_list[f'{filename}-{method_idx}'] = (list_tokens_method, list_number_line_tokens_method)

    return dicts_methods_token_list



def token_to_df(dict_methods_info, lexer='java', verbose=True):
    """
    Gets the DataFrame of tokens related to one or more scripts (samples)

    Parameters
    ----------
    dict_method_info: Dict
        Dictionary mapping each file to info (tuple of list of method codes and line of start for each method)
    lexer: str
        Type of lexer to use in order to get the tokens
    is_file: bool
        Flag specifying whether a list of scripts or a single script is to be processed. 
    verbose: bool
        Flag for showing results during preprocessing

    Returns
    ----------
    data: DataFrame
        Dataframe with each token associated with its tag and filename
    raw_code: List[str]
        List with raw code for each file 
    """

    if verbose:
        start = time.time()
        print('Starting tokenization of source code files...')
    
    if lexer=='java':
        lexer_obj = JavaLexer()
    elif lexer=='c':
        lexer_obj = CLexer()
    else:
        lexer_obj = CppLexer()

    dicts_methods_token_list = tokenization(dict_methods_info, lexer=lexer_obj)
    
    if verbose:
        print('Tokenization ready!')
        print('Tokenization time: %0.4f seconds' %(time.time()-start))

    if verbose:
        print('\nConverting tokenization to dataframe...')
        start = time.time()
        
    df = pd.DataFrame()
    #we append a DataFrame for each method
    for index_method, method_id in enumerate(dicts_methods_token_list.keys()):
        method_info = dicts_methods_token_list[method_id]
        method_token_info = method_info[0]
        method_line_numbers_tokens = method_info[1]
        list_tokens = [info[1] for info in method_token_info]
        list_tags = [info[0] for info in method_token_info]
        
        df = df.append(pd.DataFrame({'token': list_tokens, 
                                     'tags': list_tags,
                                     'line_number': method_line_numbers_tokens,
                                     'method_id': [method_id] * len(list_tokens)},
                                     index=index_method * np.ones((len(list_tokens), ),dtype=np.int16)))
    
    if verbose:
        print('Dataframe ready!')
        print('Conversion time: %0.4f seconds' %(time.time()-start))
    
    return df


def feature_extraction(data, token2idx, max_len):
    """
    Data preprocessing on tokenization DataFrame obtaining clean input for model.

    Parameters
    ----------
    data: DataFrame
        Dataframe with each token associated with its tag and method
    token2idx: Dict
        Dictionary mapping each token of the vocabulary to its index
    max_len: int
        Maximum length of input matrix for the model

    Returns
    ----------
    X: numpy.ndarray
        Input for the model
    """

    # Function to get necessary info for preprocessing input data
    agg_func = lambda s: [w for w in s['token'].values.tolist()]
    data_grouped = [method for method in data.groupby('method_id', sort=False).apply(agg_func)]
    
    # Map files to sequence of numbers and pad
    X = [[token2idx[t] if t in token2idx.keys() else token2idx['UNK'] for t in method] for method in data_grouped]
    X = pad_sequences(maxlen=max_len, sequences=X, truncating='post', padding='post', value=token2idx['PAD'])
    
    return X