# -*- coding: utf-8 -*-

import logging
import warnings
warnings.filterwarnings('ignore', category=FutureWarning)
import tensorflow as tf
import pandas as pd 
import os, json
from flask import abort
import requests

# Remove KMP_AFFINITY logs
os.environ['KMP_WARNINGS'] = 'off'
# Disables the warning "Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA", doesn't enable AVX/FMA
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
# Remove tensorflow warnings
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
# Load the logger
logger = logging.getLogger("DECODER_API.core.model")

from .imports.preprocessing_token import token_to_df, feature_extraction
from .imports.seq2seq_modules import PointerLSTM
from .code_utils import PkmHelper, DebugHelper, split_methods_java, split_methods_c, split_methods_cpp, filter_ast_dicts
from swagger_server.commons.constants import DEFAULT_RESOURCES_PATH, PKM_API_RAW_PATH, PKM_API_JAVA_SOURCE_PATH, PKM_API_C_SOURCE_PATH, PKM_API_CPP_SOURCE_PATH, JAVA_POINTER, TOKEN2IDX_JAVA, C_POINTER, TOKEN2IDX_C, MAX_LENGTH_CODE

class Model():
    """
    
    """
    def __init__(self):

        """
        Creates a Model instance, loading the trained Keras model.

        Parameters
        ----------
        None
        """
        self.base_path = DEFAULT_RESOURCES_PATH
        self.raw_pkm_url = PKM_API_RAW_PATH
        self.java_source_pkm_url = PKM_API_JAVA_SOURCE_PATH
        self.c_source_pkm_url = PKM_API_C_SOURCE_PATH
        self.cpp_source_pkm_url  = PKM_API_CPP_SOURCE_PATH

        # Java models and data
        self.java_model_name = JAVA_POINTER
        self.java_token_dict_name = TOKEN2IDX_JAVA

        try:
            with open(self.base_path + self.java_token_dict_name) as json_file:
                self.java_token2idx = json.load(json_file)
        except Exception as err:
            logger.exception('Could not open the file %s' %self.java_token_dict_name)
            abort(500)

        # Fix - Parse keys as ints
        self.java_token2idx = {key: int(value) for key, value in self.java_token2idx.items()}      

        try:
            self.java_model = tf.keras.models.load_model(self.base_path + self.java_model_name, custom_objects={'PointerLSTM': PointerLSTM})
        except Exception as err:
            logger.exception('Could not load the model %s' %self.java_model_name)
            abort(500)

        # C, C++ models and data
        self.c_model_name = C_POINTER
        self.c_token_dict_name = TOKEN2IDX_C

        try:
            with open(self.base_path + self.c_token_dict_name) as json_file:
                self.c_token2idx = json.load(json_file)
        except Exception as err:
            logger.exception('Could not open the file %s' %self.c_token_dict_name)
            abort(500)

        # Fix - Parse keys as ints
        self.c_token2idx = {key: int(value) for key, value in self.c_token2idx.items()}

        try:
            self.c_model = tf.keras.models.load_model(self.base_path + self.c_model_name, custom_objects={'PointerLSTM': PointerLSTM})
        except Exception as err:
            logger.exception('Could not load the model %s' %self.c_model_name)
            abort(500)

        self.max_len_code = MAX_LENGTH_CODE

        # Load helpers
        self.pkm = PkmHelper()
        self.debugger = DebugHelper()


    def predict(self, filename, collection, key):
        """
        Predicts NL description for the specified input.

        Parameters
        ----------
        filename: String
            Name of the source code file with extension. Admitted extensions: .java, .c or .cpp
        collection: String
            Collection name where to find the source code file
        key: String
            private key for connecting to PKM api

        Returns
        ----------
        artefact_id: String
            Path of the script to be predicted
        modified: bool
            Flag that confirms if the script has a bug
        """
        filename_encoded = requests.utils.quote(filename, safe='')
        collection_encoded = requests.utils.quote(collection, safe='')

        # Query the PKM and load the reply
        get_file_url = self.raw_pkm_url + collection_encoded + '/' + filename_encoded
        json_reply = self.pkm.get(get_file_url, key)

        # Store PKM code attributes
        rel_path = json_reply['rel_path']
        raw_source_code = json_reply['content']
        file_format = json_reply['format']
        encoding = json_reply['encoding']
        file_type = json_reply['type']
        mime_type = json_reply['mime_type']
        
        # Processing pipeline
        extension = filename.split('.')[-1].lower()
        if extension == 'java':
            token2idx = self.java_token2idx
            model = self.java_model
            source_code_url = self.java_source_pkm_url
        elif extension in ('c', 'h', 'cpp', 'hpp'):
            token2idx = self.c_token2idx
            model = self.c_model
            if extension in ('c', 'h'):
                source_code_url = self.c_source_pkm_url
            else:
                source_code_url = self.cpp_source_pkm_url
        else:
            logger.exception('Unknown programming language. The tool is able to process only .java, .cpp and .c files')
            abort(500)

        # split the whole source code in methods
        ast_url = source_code_url + collection_encoded + '/' + filename_encoded
        dict_methods_info = self._get_method_codes(ast_url,
                                                   raw_source_code,
                                                   extension,
                                                   rel_path,
                                                   key)

        try:
            tokenization_df = token_to_df(dict_methods_info, lexer=extension, verbose=True)
        except Exception as err:
            logger.exception('Error during the tokenization of the source code files')
            abort(500)

        # Vectorize and padding
        X = feature_extraction(data=tokenization_df,
                               token2idx=token2idx,
                               max_len=self.max_len_code)

        # Predict
        prediction = model.predict(X)

        # Check methods with bugs
        dict_bugs_info, modified = self.debugger.get_debug_info(dict_methods_info, prediction, tokenization_df)
        content = self.debugger.add_prediction_comments(dict_bugs_info, [raw_source_code])

        # Save the result in the PKM
        insert_file_url = self.raw_pkm_url + collection_encoded
        try:
            payload = [{'format': file_format, 'encoding': encoding, 'type': file_type, 'rel_path': rel_path, 'content': content[0], 'mime_type': mime_type}]
        except: # Avoid nulls
            payload = [{'format': 'text', 'encoding': 'utf-8', 'type': 'Code', 'rel_path': rel_path, 'content': content[0], 'mime_type': 'plain/text'}]
        
        self.pkm.put(insert_file_url, payload, key)
            
        return rel_path, modified[0]



    def predict_all(self, collection, key):
        """
        Predicts NL description for all source code files inside the specified input collection.

        Parameters
        ----------
        collection: str
            Collection name where to find the source code files
        key: str
            private key for connecting to PKM api

        Returns
        ----------
        artefacts_id: List[str]
            Path of the script to be predicted
        modified: List[bool]
            Flag that confirms if the script has a bug
        """
        # URL encode the collection
        collection_encoded = requests.utils.quote(collection, safe='')

        # Query the PKM and load the reply
        get_collection_url = self.raw_pkm_url + collection_encoded
        json_reply = self.pkm.get(get_collection_url, key)

        token2idx, model, extension, source_code_url, \
        filenames, raw_source_codes, file_formats, \
        encodings, file_types, mime_types = self._unpack_relative_variables(json_reply)
        
        # split the whole source code in methods
        ast_url = source_code_url + collection_encoded
        dict_methods_info = self._get_method_codes(ast_url,
                                                   raw_source_codes,
                                                   extension,
                                                   filenames,
                                                   key,
                                                   is_specific=False)

        # Preprocessing pipeline
        try:
            # Suppose the each raw_source_codes has an unique method
            tokenization_df = token_to_df(dict_methods_info, lexer=extension, verbose=True)
        except Exception as err:
            logger.exception('Error during the tokenization of the source code files')
            abort(500)

        # Vectorize and padding
        X = feature_extraction(data=tokenization_df,
                               token2idx=token2idx,
                               max_len=self.max_len_code)

        # Predict
        predictions = model.predict(X)

        # Check methods with bugs
        dict_bugs_info, modified = self.debugger.get_debug_info(dict_methods_info, predictions, tokenization_df)
        content = self.debugger.add_prediction_comments(dict_bugs_info, raw_source_codes)

        # Save the result in the PKM
        insert_file_url = self.raw_pkm_url + collection_encoded
        payload = []
        for index, output_source_code in enumerate(content):
            try:
                payload.append({'format': file_formats[index], 'encoding': encodings[index], 'type': file_types[index], 'rel_path': filenames[index], 'content': output_source_code, 'mime_type': mime_types[index]})
            except: # Avoid nulls
                payload.append({'format': 'text', 'encoding': 'utf-8', 'type': 'Code', 'rel_path': filenames[index], 'content': output_source_code, 'mime_type': 'plain/text'})

        self.pkm.put(insert_file_url, payload, key)
    
        return filenames, modified


    def _get_method_codes(self, ast_url, raw_source_code, extension, rel_path, key, is_specific=True):
        """
        Split methods of files depending on its programming language

        Parameters
        ----------
        ast_url: str
            Url for GET request in order to get the AST of each script
        raw_source_code: List[str]_or_str
            Source code of each filename/es
		extension: str
            extension depending on programming language
        rel_path: List[str]_or_str
            relative path/ths of each filename/es
        key: str
            private key for connecting to PKM api
        is_specific: bool
            flag that specifies if the splitting is for one or more files

        Returns
        ----------
        dict_method_info: Dict
        Dictionary mapping each file to info (tuple of list of method codes and line of start for each method)
        """

        if is_specific:
            rel_path = [rel_path]
            raw_source_code = [raw_source_code]

        try:
            # Query the PKM for extracting the methods and load the reply
            ast_dicts = self.pkm.get(ast_url, key)
            filtered_ast_dicts = filter_ast_dicts(ast_dicts, rel_path)
            if extension == 'java':
                dict_methods_info = split_methods_java(filtered_ast_dicts, raw_source_code)
            elif extension == 'c': 
                dict_methods_info = split_methods_c(filtered_ast_dicts, raw_source_code)
            else:
                dict_methods_info = split_methods_cpp(filtered_ast_dicts, raw_source_code)
                
        except Exception as err:
            logger.warning('Error in parsing the reply from endpoint %s. Analyzing the entire file ...' %ast_url)
            if isinstance(rel_path, str):
                dict_methods_info = {rel_path :[(raw_source_code, 0)]}
            else:
                dict_methods_info = {r_path : [(r_source_code, 0)] for r_path, r_source_code in zip(rel_path, raw_source_code)}

        return dict_methods_info

    def _unpack_relative_variables(self, json_reply):
        """
        Unpack all data from the json response and return only the relative information of the referenced language

        Parameters
        ----------
        json_reply: dict
            Json con los datos de la respuesta de PKM relacionados con los archivos de script

        Returns
        ----------
        token2idx: Dict
            Dictionary mapping each token of the vocabulary to its index
        model: tensorflow.keras.Model
            Model for inference
        extension: str
            Extension of the script files for identifying the programming language
        source_code_url: str
            Url base to ask PKM about ASTs
        filenames: List[str]
            Lists of valid script files for inference
        raw_source_codes: List[str]
            Lists of raw source codes for valid script files
        file_formats: List[str]
            File formats related to valid script files
        encoding: List[str]
            Encoding type related to valid script files
        file_types: List[str]
            File type related to valid script files
        mime_types: List[str]
            Mime type related to valid script files
        """

        df_json_info = pd.DataFrame(json_reply)

        df_json_info['extension'] = df_json_info['rel_path'].apply(lambda x: x.split('.')[-1])

        #take information for each language
        df_java_data = df_json_info[df_json_info['extension'] == 'java']
        df_c_data = df_json_info[df_json_info['extension'].isin(['c', 'h'])]
        df_cpp_data = df_json_info[df_json_info['extension'].isin(['cpp', 'hpp'])]

        # check if files in the project are java, c or cpp

        relevant_files =  df_java_data.shape[0] + df_c_data.shape[0] + df_cpp_data.shape[0]

        if not relevant_files:
            logger.error('No relevant file found. The tool is able to process only .java, .cpp/.hpp and .c/.h files.')
            abort(500)
        
        # Set variables depending on programming language
        if not df_java_data.empty:
            token2idx = self.java_token2idx
            model = self.java_model
            extension = 'java'
            source_code_url = self.java_source_pkm_url
            df_relative = df_java_data.copy()
        else:
            token2idx = self.c_token2idx
            model = self.c_model
            if not df_c_data.empty: 
                extension='c'
                source_code_url = self.c_source_pkm_url
                df_relative = df_c_data.copy()
            else:
                extension='cpp'
                source_code_url = self.cpp_source_pkm_url
                df_relative = df_cpp_data.copy()

        filenames=df_relative['rel_path'].values.tolist()
        raw_source_codes=df_relative['content'].values.tolist()
        file_formats=df_relative['format'].values.tolist()
        encodings=df_relative['encoding'].values.tolist()
        file_types=df_relative['type'].values.tolist()
        mime_types=df_relative['mime_type'].values.tolist()

        return token2idx, model, extension, source_code_url, filenames, raw_source_codes, file_formats, encodings, file_types, mime_types