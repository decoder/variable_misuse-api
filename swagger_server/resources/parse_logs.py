# -*- coding: utf-8 -*-

import re
from datetime import datetime as dt
import requests
import logging

# Remove requests warnings
requests.packages.urllib3.disable_warnings()
# Load the logger
logger = logging.getLogger("DECODER_API.core.logs")

from swagger_server.commons.constants import PKM_API_BASE_URL, DEFAULT_RESOURCES_PATH


def matchDate(line):
    matchThis = ""
    matched = re.match(r"\[\d\d\d\d-\d\d-\d\d\ \d\d:\d\d:\d\d", line)
    if matched:
        # Matches a date and adds it to matchThis
        matchThis = matched.group()
    else:
        matchThis = "NONE"
    return matchThis


def check_reply(reply):
    if reply.ok:
        try:
            return reply.json()
        except:
            pass
    else:
        # If response code is not ok (200) raise the error
        reply.raise_for_status()


def upload_logs(start_date, collection, key):
    try:
        # Initialize default parameters
        tool = "variable_misuse"
        nature_of_report = "Execution log"
        type = "Log"
        start_time = start_date.strftime(
            "%Y-%m-%d %H:%M:%S"
        )  # String format up to seconds
        start_date = dt.strptime(
            start_time, "%Y-%m-%d %H:%M:%S"
        )  # Datetime format up to seconds
        messages = []
        warnings = []
        errors = []

        with open("./swagger_server/logs/DECODER_API.log") as f:
            for line in f:
                if line.startswith(matchDate(line)):
                    end_time = line.split("] - ")[0][1:20]
                    end_date = dt.strptime(end_time, "%Y-%m-%d %H:%M:%S")
                    if end_date >= start_date:
                        line_split = line.split(" - ", 3)
                        log_type = line_split[1]
                        text = line_split[-1][:-1]

                        if log_type.lower() == "error":
                            errors.append(text)
                        elif log_type.lower() == "warning":
                            warnings.append(text)
                        else:
                            messages.append(text)

        if len(errors) == 0:
            status = True
        else:
            status = False

        # Push the logs to the PKM
        insert_log_url = PKM_API_BASE_URL + "/log/" + collection
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "key": key,
        }
        payload = [
            {
                "tool": tool,
                "nature of report": nature_of_report,
                "type": type,
                "start running time": start_time,
                "end running time": end_time,
                "messages": messages,
                "warnings": warnings,
                "errors": errors,
                "status": status,
            }
        ]

        log_insert_response = requests.post(
            insert_log_url,
            json=payload,
            headers=headers,
            verify=DEFAULT_RESOURCES_PATH + "pkm.crt",
        )
        # this will raise an expection if not right
        check_reply(log_insert_response)

        # get message and status
        message = log_insert_response.json()

    except requests.exceptions.HTTPError as err:
        message = None
        status = False
        logger.warning(message)
        logger.exception(err)

    return status, message


def update_invocation(collection, key, invocation_id, status, invocation_results=None):

    request_headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "key": key,
    }
    get_invocation_url = (
        PKM_API_BASE_URL + "/invocations/" + collection + "/" + invocation_id
    )
    get_invocation_result = requests.get(
        get_invocation_url,
        headers=request_headers,
        verify=DEFAULT_RESOURCES_PATH + "pkm.crt",
    )

    try:
        get_invocation_result.raise_for_status()
        invocation_info = get_invocation_result.json()
    except requests.exceptions.HTTPError as err:
        logging.warning(f"Could not get invocation {invocation_id} info from PKM")
        logging.exception(err)
        return None

    if invocation_results is None:
        invocation_results = []

    invocation_update = {"invocationID": invocation_id}

    if status:
        invocation_update["invocationStatus"] = "COMPLETED"
    else:
        invocation_update["invocationStatus"] = "FAILED"
        invocation_results.append({"path": "Failed execution", "type": "message"})

    invocation_update["invocationResults"] = invocation_results

    invocation_update["timestampCompleted"] = dt.now().strftime("%Y%m%d_%H%M%S")
    invocation_info.update(invocation_update)

    put_invocation_url = PKM_API_BASE_URL + "/invocations/" + collection
    put_invocation_result = requests.put(
        put_invocation_url,
        json=[invocation_info],
        headers=request_headers,
        verify=DEFAULT_RESOURCES_PATH + "pkm.crt",
    )

    try:
        put_invocation_result.raise_for_status()
    except requests.exceptions.HTTPError as err:
        logging.warning(f"Could not put invocation {invocation_id} info into PKM")
        logging.exception(err)
        return None
