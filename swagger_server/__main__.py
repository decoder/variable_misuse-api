# -*- coding: utf-8 -*-

import connexion
from flask import make_response, jsonify
import logging
import os
from dotenv import load_dotenv

from swagger_server import encoder
from swagger_server.commons.constants import LOGGING_LEVEL, LOG_PATH



def custom_error(message, status_code): 
    return make_response(jsonify(message), status_code)

def handle_not_found(e):
    msg = 'A source code file with the specified artefactId was not found in the PKM'
    return custom_error(msg, 404)

def handle_internal_error(e):
    return custom_error('Internal application error', 500)



def main():
    # Configure the logger
    logger = logging.getLogger('DECODER_API')
    logger.setLevel(LOGGING_LEVEL)
    formatter = logging.Formatter("[%(asctime)s] - %(levelname)s - %(name)s: %(message)s")

    # create directory if it does not not exist
    if not os.path.exists(os.path.dirname(LOG_PATH)):
        os.makedirs(os.path.dirname(LOG_PATH))
    
    fileHandler = logging.FileHandler(LOG_PATH, mode='w')    # Create file handler deleting previous logs
    fileHandler.setLevel(LOGGING_LEVEL)
    fileHandler.setFormatter(formatter)
    logger.addHandler(fileHandler)

    ch = logging.StreamHandler()                       # Create console handler
    ch.setLevel(LOGGING_LEVEL)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    # Launch the API
    load_dotenv() # Load environment variables from .env file
    port = os.environ['PORT'] # Take the port for exposing the API from the environment variable
    logger.info('Starting variable misuse API in port %s' %port)
    logger.info('To open documentation click on: http://localhost:%s/apt/ui/' %port)
    app = connexion.App(__name__, specification_dir='./swagger/')
    app.app.logger.addHandler(fileHandler)    # Add console and file handlers for app
    app.app.logger.addHandler(ch)
    app.app.logger.disabled = True
    log = logging.getLogger('werkzeug')
    log.disabled = True                       # Disable the logs for werkzeug

    # Error handling
    app.app.register_error_handler(404, handle_not_found)
    app.app.register_error_handler(500, handle_internal_error)

    app.app.json_encoder = encoder.JSONEncoder
    app.add_api('swagger.yaml', arguments={'title': 'WP2_parse_source_code'}, pythonic_params=True)
    app.run(port=port, threaded=True)
    logger.info('Variable misuse API stopped')


if __name__ == '__main__':
    main()

